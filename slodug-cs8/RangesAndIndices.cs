﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlodugCs8
{
    class RangesAndIndices
    {
        [Test]
        public void RangeFromBeginning()
        {
            Range range = 1..5;

            var array = new[] { 0, 1, 2, 3, 4, 5 };
            var subArray = array[range];

            Assert.AreEqual(new int[] { 1, 2, 3, 4 }, subArray);
        }

        [Test]
        public void RangeFromEnd()
        {
            Range range = 1..^1;

            var array = new[] { 0, 1, 2, 3, 4, 5 };
            var subArray = array[range];

            Assert.AreEqual(new int[] { 1, 2, 3, 4 }, subArray);
        }

        [Test]
        public void OpenEndedBeginning()
        {

            var array = new[] { 0, 1, 2, 3, 4, 5 };
            var subArray = array[..^1];

            Assert.AreEqual(new int[] { 0, 1, 2, 3, 4 }, subArray);
        }

        [Test]
        public void OpenEndedEnd()
        {
            var array = new[] { 0, 1, 2, 3, 4, 5 };
            var subArray = array[1..];

            Assert.AreEqual(new int[] { 1, 2, 3, 4, 5 }, subArray);
        }

        [Test]
        public void IndexFromBeginning()
        {
            var array = new[] { 0, 1, 2, 3, 4, 5 };
            var item = array[1];

            Assert.AreEqual(1, item);
        }

        [Test]
        public void IndexFromEnd()
        {
            var array = new[] { 0, 1, 2, 3, 4, 5 };
            var item = array[^1];

            Assert.AreEqual(5, item);
        }

        [Test]
        public void ImplicitIndexerForRange()
        {
            var substring = "012345"[1..^1];

            Assert.AreEqual("1234", substring);
        }

        [Test]
        public void ImplicitIndexerForIndex()
        {
            var character = "012345"[^1];

            Assert.AreEqual('5', character);
        }
    }
}
