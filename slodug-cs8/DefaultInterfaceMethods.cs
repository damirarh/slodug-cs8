﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SlodugCs8
{
    class DefaultInterfaceMethods
    {
        interface I0
        {
            string Method()
            {
                return "I0";
            }
        }

        abstract class A1 : I0
        {
            public string Method()
            {
                return "A1";
            }
        }

        abstract class A2 : I0
        {
            public string Method()
            {
                return "A2";
            }
        }

        interface I1 : I0
        {
            string I0.Method()
            {
                return "I1";
            }
        }

        interface I2 : I0
        {
            string I0.Method()
            {
                return "I2";
            }
        }

        public class C0 : I0
        { }

        [Test]
        public void InvokeDefaultMethod()
        {
            // not available on class
            //var instance = new C0();
            I0 instance = new C0();

            Assert.AreEqual("I0", instance.Method());
        }

        class C12 : I1, I2
        {
            string I0.Method()
            {
                return "C12";
            }
        }

        [Test]
        public void MultipleInheritance()
        {
            I0 instance = new C12();

            Assert.AreEqual("C12", instance.Method());
        }
    }
}
