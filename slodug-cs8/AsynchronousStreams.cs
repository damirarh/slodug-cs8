﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SlodugCs8
{
    class AsynchronousStreams
    {
        private async IAsyncEnumerable<int> GetValuesAsync()
        {
            for (var i = 0; i < 10; i++)
            {
                await Task.Delay(100);
                yield return i;
            }
        }

        [Test]
        public async Task AsyncForeach()
        {
            await foreach (var value in GetValuesAsync())
            {
                // process value
            }

            Assert.Pass();
        }

    }
}
