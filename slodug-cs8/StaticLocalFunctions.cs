using NUnit.Framework;

namespace SlodugCs8
{
    public class StaticLocalFunctions
    {
        [Test]
        public void InstanceLocalFunction()
        {
            int i = 42;
            InstanceLocalFunc();

            void InstanceLocalFunc()
            {
                Assert.AreEqual(42, i);
            }
        }

        [Test]
        public void StaticLocalFunction()
        {
            int i = 42;
            StaticLocalFunc(i);

            static void StaticLocalFunc(int p)
            {
                // no access to i
                //Assert.AreEqual(42, i);
                Assert.AreEqual(42, p);
            }
        }
    }
}